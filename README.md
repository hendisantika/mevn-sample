# mevn-sample

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Screenshot

Home Page

![Home Page](img/home.png "Home Page")

Create New Post

![Create New Post](img/create.png "Create New Posting")

List Posting

![List Posting](img/list.png "List Posting")

Edit Posting

![Edit Posting](img/edit.png "Edit Posting")
